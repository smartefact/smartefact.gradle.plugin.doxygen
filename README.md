[![Latest release](https://gitlab.com/smartefact/smartefact.gradle.plugin.doxygen/-/badges/release.svg?key_text=Latest%20release&key_width=100)](https://gitlab.com/smartefact/smartefact.gradle.plugin.doxygen/-/releases)
[![Pipeline status](https://gitlab.com/smartefact/smartefact.gradle.plugin.doxygen/badges/main/pipeline.svg?key_text=Pipeline%20status&key_width=100)](https://gitlab.com/smartefact/smartefact.gradle.plugin.doxygen/-/commits/main)

# smartefact.gradle.plugin.doxygen

[Gradle](https://gradle.org/) plugin to use [Doxygen](https://doxygen.nl/).

## Usage

Add the Smartefact Maven repository to the plugin management.

`settings.gradle.kts`

```kotlin
pluginManagement {
    repositories {
        // Gradle Portal
        gradlePluginPortal()
        // Smartefact Maven repository
        maven("https://gitlab.com/api/v4/groups/smartefact/-/packages/maven")
        // Other repositories...
    }
}
```

Apply the Smartefact Doxygen plugin.

`build.gradle.kts`

```kotlin
plugins {
    id("smartefact.doxygen") version "0.3.0"
    // Other plugins...
}
```

Configure the Doxygen extension.

`build.gradle.kts`

```kotlin
doxygen {
    inlineInheritedMembers.set(true)
    // Other properties...
}
```

[More about the Doxygen extension...](#doxygen-extension)

Create a Doxygen task and configure it.

`build.gradle.kts`

```kotlin
val doxygen by tasks.registering(DoxygenTask::class) {
    input.from("sources1", "sources2")
    // Other properties...
}
```

[More about the Doxygen tasks...](#doxygen-tasks)

Run the Doxygen task.

```shell
./gradlew doxygen
```

## Doxygen configuration properties

The `doxygen` extension and most Doxygen tasks (see below) have the following properties:

| Property                     | Description                                                                                           | Default value              |
|------------------------------|-------------------------------------------------------------------------------------------------------|----------------------------|
| `doxyfile`                   | Doxyfile included at the beginning of the generated configuration file                                | `Doxyfile` if it exists    |
| `projectName`                | Project name                                                                                          | Gradle project name        |
| `projectNumber`              | Project number, a.k.a. version                                                                        | Gradle project version     |
| `projectBrief`               | Project brief, a.k.a. description                                                                     | Gradle project description |
| `projectLogo`                | Project logo                                                                                          |                            |
| `projectIcon`                | Project icon                                                                                          |                            |
| `outputDirectory`            | Output directory                                                                                      | `$buildDir/doxygen`        |
| `createSubdirectories`       | Whether to create output subdirectories                                                               |                            |
| `createSubdirectoriesLevel`  | Number of created output subdirectories                                                               |                            |
| `allowUnicodeNames`          | Whether to allow non-ASCII characters in generated file names                                         |                            |
| `outputLanguage`             | Output language                                                                                       |                            |
| `briefMemberDescription`     | Whether to include the brief member descriptions after members listed in file and class documentation |                            |
| `repeatBriefDescription`     | Whether to prepend the brief description of a member before its detailed description                  |                            |
| `abbreviateBriefDescription` | Whether to abbreviate the brief descriptions                                                          |                            |
| `alwaysDetailedSection`      | Whether to always generate a detailed section                                                         |                            |
| `inlineInheritedMembers`     | Whether to include inherited members in class documentation                                           |                            |
| `fullPathNames`              | Whether to prepend the full path before file names in the file list                                   |                            |
| `stripFromPath`              | Paths to be stripped from the full path                                                               |                            |
| `stripFromIncPath`           | Paths to be stripped from the include path                                                            |                            |
| `javadocAutoBrief`           | Whether to interpret the first line of Javadoc-style comments as the brief description                |                            |
| `javadocBanner`              | Whether to interpret comments starting with more than two asterisks as Javadoc-style comments         |                            |
| `inheritDocs`                | Whether undocumented members inherit the documentation from the members they implement                |                            |
| `optimizeOutputJava`         | Whether to optimize output for Java or Python sources                                                 |                            |
| `markdownSupport`            | Whether to support Markdown in comments                                                               |                            |
| `autolinkSupport`            | Whether to try to link words that correspond to classes or namespaces to their documentation          |                            |
| `extractAll`                 | Whether to assume all entities are documented                                                         |                            |
| `extractPrivate`             | Whether to include private members of classes in the documentation                                    |                            |
| `extractPackage`             | Whether to include package or internal members in the documentation                                   |                            |
| `showFiles`                  | Whether to generate the *Files* page                                                                  |                            |
| `quiet`                      | Whether to disable messages generated to standard output by Doxygen                                   |                            |
| `warnings`                   | Whether to disable warning messages generated to standard error by Doxygen                            |                            |
| `inputEncoding`              | Encoding of the input files                                                                           |                            |
| `filePatterns`               | Input file patterns                                                                                   |                            |
| `recursive`                  | Whether to search subdirectories for input files                                                      |                            |
| `sourceBrowser`              | Whether to generate a list of source files                                                            |                            |
| `inlineSources`              | Whether to include the sources in the documentation                                                   |                            |
| `generateHtml`               | Whether to generate HTML output                                                                       |                            |
| `generateLatex`              | Whether to generate LaTeX output                                                                      |                            |
| `enablePreprocessing`        | Whether to evaluate C-preprocessor directives in source and include files                             |                            |
| `tags`                       | Doxygen tags                                                                                          |                            |

Generally, properties correspond to Doxygen tags.

[More about the Doxygen tags...](https://www.doxygen.nl/manual/config.html)

## Doxygen extension

The `doxygen` extension allows to define properties shared/extended by all Doxygen tasks.

It has the following additional properties:

| Property  | Description       | Default value      |
|-----------|-------------------|--------------------|
| `command` | `doxygen` command | `/usr/bin/doxygen` |

## Doxygen tasks

### `DoxygenTask`

`DoxygenTask` calls Doxygen to generate documentation.

It inherits all the properties from the Doxygen extension,
and adds the following properties:

| Property | Description                 | Default value |
|----------|-----------------------------|---------------|
| `input`  | Input files and directories |               |

If the `input` property is empty, the Doxygen task will be skipped.

### `DoxygenGenerateDefaultHtml`

`DoxygenGenerateDefaultHtmlTask` calls Doxygen to generate the default HTML files:
`header.html`, `footer.html` and `stylesheet.css`.
These files can be used as base for custom files.

`DoxygenGenerateDefaultHtmlTask` inherits all the properties from the Doxygen extension,
and adds the following properties:

| Property               | Description                                 | Default value                    |
|------------------------|---------------------------------------------|----------------------------------|
| `defaultHtmlDirectory` | Output directory for the default HTML files | `$buildDir/doxygen/default-html` |

The Doxygen plugin automatically creates a `doxygenGenerateDefaultHtml` task.

## Interactions with other plugins

### Java

When the `java` plugin is applied, a `doxygen` task is automatically created with the following properties:

| Property                 | Value                                                                                   |
|--------------------------|-----------------------------------------------------------------------------------------|
| `outputDirectory`        | `$docsDir/doxygen`                                                                      |
| `briefMemberDescription` | `true`                                                                                  |
| `fullPathNames`          | `false`                                                                                 |
| `repeatBriefDescription` | `true`                                                                                  |
| `alwaysDetailedSection`  | `false`                                                                                 |
| `javadocAutoBrief`       | `true`                                                                                  |
| `javadocBanner`          | `true`                                                                                  |
| `optimizeOutputJava`     | `true`                                                                                  |
| `markdownSupport`        | `false`                                                                                 |
| `autolinkSupport`        | `false`                                                                                 |
| `extractAll`             | `true`                                                                                  |
| `showFiles`              | `false`                                                                                 |
| `input`                  | All existing main Java source directories, and the `src/doxygen` directory if it exists |
| `inputEncoding`          | Encoding of the main Java sources                                                       |
| `filePatterns`           | `*.java`, `*.md`, `*.markdown`                                                          |
| `recursive`              | `true`                                                                                  |
| `generateHtml`           | `true`                                                                                  |
| `generateLatex`          | `false`                                                                                 |
| `enablePreprocessing`    | `false`                                                                                 |

The following tags are also set:

| Tag                      | Value   |
|--------------------------|---------|
| `DIRECTORY_GRAPH`        | `false` |
| `INLINE_SIMPLE_STRUCTS`  | `false` |
| `SEPARATE_MEMBER_PAGES`  | `false` |
| `SHOW_USED_FILES`        | `false` |
| `SORT_BRIEF_DOCS`        | `true`  |
| `SORT_MEMBER_DOCS`       | `true`  |
| `SORT_MEMBERS_CTORS_1ST` | `true`  |
| `SUBGROUPING`            | `false` |

Tip: the `doxygen` task can be added as a dependency of the `assemble` task.

## Roadmap

### Tags

The following Doxygen tags should be exposed as properties:

- [x] `ABBREVIATE_BRIEF`
- [ ] `ALIASES`
- [ ] `ALLEXTERNALS`
- [x] `ALLOW_UNICODE_NAMES`
- [ ] `ALPHABETICAL_INDEX`
- [x] `ALWAYS_DETAILED_SEC`
- [x] `AUTOLINK_SUPPORT`
- [ ] `BINARY_TOC`
- [x] `BRIEF_MEMBER_DESC`
- [ ] `BUILTIN_STL_SUPPORT`
- [ ] `CALLER_GRAPH`
- [ ] `CALL_GRAPH`
- [ ] `CASE_SENSE_NAMES`
- [ ] `CHM_FILE`
- [ ] `CHM_INDEX_ENCODING`
- [ ] `CITE_BIB_FILES`
- [ ] `CLANG_ADD_INC_PATHS`
- [ ] `CLANG_ASSISTED_PARSING`
- [ ] `CLANG_DATABASE_PATH`
- [ ] `CLANG_OPTIONS`
- [ ] `CLASS_GRAPH`
- [ ] `COLLABORATION_GRAPH`
- [ ] `COMPACT_LATEX`
- [ ] `COMPACT_RTF`
- [ ] `CPP_CLI_SUPPORT`
- [x] `CREATE_SUBDIRS`
- [x] `CREATE_SUBDIRS_LEVEL`
- [ ] `DIAFILE_DIRS`
- [ ] `DIA_PATH`
- [ ] `DIRECTORY_GRAPH`
- [ ] `DIR_GRAPH_MAX_DEPTH`
- [ ] `DISABLE_INDEX`
- [ ] `DISTRIBUTE_GROUP_DOC`
- [ ] `DOCBOOK_OUTPUT`
- [ ] `DOCSET_BUNDLE_ID`
- [ ] `DOCSET_FEEDNAME`
- [ ] `DOCSET_FEEDURL`
- [ ] `DOCSET_PUBLISHER_ID`
- [ ] `DOCSET_PUBLISHER_NAME`
- [ ] `DOTFILE_DIRS`
- [ ] `DOT_CLEANUP`
- [ ] `DOT_COMMON_ATTR`
- [ ] `DOT_EDGE_ATTR`
- [ ] `DOT_FONTPATH`
- [ ] `DOT_GRAPH_MAX_NODES`
- [ ] `DOT_IMAGE_FORMAT`
- [ ] `DOT_MULTI_TARGETS`
- [ ] `DOT_NODE_ATTR`
- [ ] `DOT_NUM_THREADS`
- [ ] `DOT_PATH`
- [ ] `DOT_UML_DETAILS`
- [ ] `DOT_WRAP_THRESHOLD`
- [ ] `DOXYFILE_ENCODING`
- [ ] `ECLIPSE_DOC_ID`
- [ ] `ENABLED_SECTIONS`
- [ ] `ENABLE_PREPROCESSING`
- [ ] `ENUM_VALUES_PER_LINE`
- [ ] `EXAMPLE_PATH`
- [ ] `EXAMPLE_PATTERNS`
- [ ] `EXAMPLE_RECURSIVE`
- [ ] `EXCLUDE`
- [ ] `EXCLUDE_PATTERNS`
- [ ] `EXCLUDE_SYMBOLS`
- [ ] `EXCLUDE_SYMLINKS`
- [ ] `EXPAND_AS_DEFINED`
- [ ] `EXPAND_ONLY_PREDEF`
- [ ] `EXTENSION_MAPPING`
- [ ] `EXTERNAL_GROUPS`
- [ ] `EXTERNAL_PAGES`
- [ ] `EXTERNAL_SEARCH`
- [ ] `EXTERNAL_SEARCH_ID`
- [x] `EXTRACT_ALL`
- [ ] `EXTRACT_ANON_NSPACES`
- [ ] `EXTRACT_LOCAL_CLASSES`
- [ ] `EXTRACT_LOCAL_METHODS`
- [x] `EXTRACT_PACKAGE`
- [x] `EXTRACT_PRIVATE`
- [ ] `EXTRACT_PRIV_VIRTUAL`
- [ ] `EXTRACT_STATIC`
- [ ] `EXTRA_PACKAGES`
- [ ] `EXTRA_SEARCH_MAPPINGS`
- [ ] `EXT_LINKS_IN_WINDOW`
- [x] `FILE_PATTERNS`
- [ ] `FILE_VERSION_FILTER`
- [ ] `FILTER_PATTERNS`
- [ ] `FILTER_SOURCE_FILES`
- [ ] `FILTER_SOURCE_PATTERNS`
- [ ] `FORCE_LOCAL_INCLUDES`
- [ ] `FORMULA_FONTSIZE`
- [ ] `FORMULA_MACROFILE`
- [ ] `FORTRAN_COMMENT_AFTER`
- [x] `FULL_PATH_NAMES`
- [ ] `FULL_SIDEBAR`
- [ ] `GENERATE_AUTOGEN_DEF`
- [ ] `GENERATE_BUGLIST`
- [ ] `GENERATE_CHI`
- [ ] `GENERATE_DEPRECATEDLIST`
- [ ] `GENERATE_DOCBOOK`
- [ ] `GENERATE_DOCSET`
- [ ] `GENERATE_ECLIPSEHELP`
- [x] `GENERATE_HTML`
- [ ] `GENERATE_HTMLHELP`
- [x] `GENERATE_LATEX`
- [ ] `GENERATE_LEGEND`
- [ ] `GENERATE_MAN`
- [ ] `GENERATE_PERLMOD`
- [ ] `GENERATE_QHP`
- [ ] `GENERATE_RTF`
- [ ] `GENERATE_SQLITE3`
- [ ] `GENERATE_TAGFILE`
- [ ] `GENERATE_TESTLIST`
- [ ] `GENERATE_TODOLIST`
- [ ] `GENERATE_TREEVIEW`
- [ ] `GENERATE_XML`
- [ ] `GRAPHICAL_HIERARCHY`
- [ ] `GROUP_GRAPHS`
- [ ] `GROUP_NESTED_COMPOUNDS`
- [ ] `HAVE_DOT`
- [ ] `HHC_LOCATION`
- [ ] `HIDE_COMPOUND_REFERENCE`
- [ ] `HIDE_FRIEND_COMPOUNDS`
- [ ] `HIDE_IN_BODY_DOCS`
- [ ] `HIDE_SCOPE_NAMES`
- [ ] `HIDE_UNDOC_CLASSES`
- [ ] `HIDE_UNDOC_MEMBERS`
- [ ] `HIDE_UNDOC_RELATIONS`
- [ ] `HTML_CODE_FOLDING`
- [ ] `HTML_COLORSTYLE`
- [ ] `HTML_COLORSTYLE_GAMMA`
- [ ] `HTML_COLORSTYLE_HUE`
- [ ] `HTML_COLORSTYLE_SAT`
- [ ] `HTML_COPY_CLIPBOARD`
- [ ] `HTML_DYNAMIC_MENUS`
- [ ] `HTML_DYNAMIC_SECTIONS`
- [ ] `HTML_EXTRA_FILES`
- [ ] `HTML_EXTRA_STYLESHEET`
- [ ] `HTML_FILE_EXTENSION`
- [ ] `HTML_FOOTER`
- [ ] `HTML_FORMULA_FORMAT`
- [ ] `HTML_HEADER`
- [ ] `HTML_INDEX_NUM_ENTRIES`
- [ ] `HTML_OUTPUT`
- [ ] `HTML_PROJECT_COOKIE`
- [ ] `HTML_STYLESHEET`
- [ ] `IDL_PROPERTY_SUPPORT`
- [ ] `IGNORE_PREFIX`
- [ ] `IMAGE_PATH`
- [ ] `INCLUDED_BY_GRAPH`
- [ ] `INCLUDE_FILE_PATTERNS`
- [ ] `INCLUDE_GRAPH`
- [ ] `INCLUDE_PATH`
- [x] `INHERIT_DOCS`
- [ ] `INLINE_GROUPED_CLASSES`
- [ ] `INLINE_INFO`
- [x] `INLINE_INHERITED_MEMB`
- [ ] `INLINE_SIMPLE_STRUCTS`
- [x] `INLINE_SOURCES`
- [x] `INPUT`
- [x] `INPUT_ENCODING`
- [ ] `INPUT_FILE_ENCODING`
- [ ] `INPUT_FILTER`
- [ ] `INTERACTIVE_SVG`
- [ ] `INTERNAL_DOCS`
- [x] `JAVADOC_AUTOBRIEF`
- [x] `JAVADOC_BANNER`
- [ ] `LATEX_BATCHMODE`
- [ ] `LATEX_BIB_STYLE`
- [ ] `LATEX_CMD_NAME`
- [ ] `LATEX_EMOJI_DIRECTORY`
- [ ] `LATEX_EXTRA_FILES`
- [ ] `LATEX_EXTRA_STYLESHEET`
- [ ] `LATEX_FOOTER`
- [ ] `LATEX_HEADER`
- [ ] `LATEX_HIDE_INDICES`
- [ ] `LATEX_MAKEINDEX_CMD`
- [ ] `LATEX_OUTPUT`
- [ ] `LAYOUT_FILE`
- [ ] `LOOKUP_CACHE_SIZE`
- [ ] `MACRO_EXPANSION`
- [ ] `MAKEINDEX_CMD_NAME`
- [ ] `MAN_EXTENSION`
- [ ] `MAN_LINKS`
- [ ] `MAN_OUTPUT`
- [ ] `MAN_SUBDIR`
- [ ] `MARKDOWN_ID_STYLE`
- [x] `MARKDOWN_SUPPORT`
- [ ] `MATHJAX_CODEFILE`
- [ ] `MATHJAX_EXTENSIONS`
- [ ] `MATHJAX_FORMAT`
- [ ] `MATHJAX_RELPATH`
- [ ] `MATHJAX_VERSION`
- [ ] `MAX_DOT_GRAPH_DEPTH`
- [ ] `MAX_INITIALIZER_LINES`
- [ ] `MSCFILE_DIRS`
- [ ] `MSCGEN_TOOL`
- [ ] `MULTILINE_CPP_IS_BRIEF`
- [ ] `NUM_PROC_THREADS`
- [ ] `OBFUSCATE_EMAILS`
- [ ] `OPTIMIZE_FOR_FORTRAN`
- [ ] `OPTIMIZE_OUTPUT_FOR_C`
- [x] `OPTIMIZE_OUTPUT_JAVA`
- [ ] `OPTIMIZE_OUTPUT_SLICE`
- [ ] `OPTIMIZE_OUTPUT_VHDL`
- [x] `OUTPUT_DIRECTORY`
- [x] `OUTPUT_LANGUAGE`
- [ ] `PAPER_TYPE`
- [ ] `PDF_HYPERLINKS`
- [ ] `PERLMOD_LATEX`
- [ ] `PERLMOD_MAKEVAR_PREFIX`
- [ ] `PERLMOD_PRETTY`
- [ ] `PLANTUML_CFG_FILE`
- [ ] `PLANTUML_INCLUDE_PATH`
- [ ] `PLANTUML_JAR_PATH`
- [ ] `PREDEFINED`
- [x] `PROJECT_BRIEF`
- [x] `PROJECT_ICON`
- [x] `PROJECT_LOGO`
- [x] `PROJECT_NAME`
- [x] `PROJECT_NUMBER`
- [ ] `PYTHON_DOCSTRING`
- [ ] `QCH_FILE`
- [ ] `QHG_LOCATION`
- [ ] `QHP_CUST_FILTER_ATTRS`
- [ ] `QHP_CUST_FILTER_NAME`
- [ ] `QHP_NAMESPACE`
- [ ] `QHP_SECT_FILTER_ATTRS`
- [ ] `QHP_VIRTUAL_FOLDER`
- [ ] `QT_AUTOBRIEF`
- [x] `QUIET`
- [x] `RECURSIVE`
- [ ] `REFERENCED_BY_RELATION`
- [ ] `REFERENCES_LINK_SOURCE`
- [ ] `REFERENCES_RELATION`
- [x] `REPEAT_BRIEF`
- [ ] `RESOLVE_UNNAMED_PARAMS`
- [ ] `RTF_EXTENSIONS_FILE`
- [ ] `RTF_HYPERLINKS`
- [ ] `RTF_OUTPUT`
- [ ] `RTF_STYLESHEET_FILE`
- [ ] `SEARCHDATA_FILE`
- [ ] `SEARCHENGINE`
- [ ] `SEARCHENGINE_URL`
- [ ] `SEARCH_INCLUDES`
- [ ] `SEPARATE_MEMBER_PAGES`
- [ ] `SERVER_BASED_SEARCH`
- [ ] `SHORT_NAMES`
- [x] `SHOW_FILES`
- [ ] `SHOW_GROUPED_MEMB_INC`
- [ ] `SHOW_HEADERFILE`
- [ ] `SHOW_INCLUDE_FILES`
- [ ] `SHOW_NAMESPACES`
- [ ] `SHOW_USED_FILES`
- [ ] `SIP_SUPPORT`
- [ ] `SITEMAP_URL`
- [ ] `SKIP_FUNCTION_MACROS`
- [ ] `SORT_BRIEF_DOCS`
- [ ] `SORT_BY_SCOPE_NAME`
- [ ] `SORT_GROUP_NAMES`
- [ ] `SORT_MEMBERS_CTORS_1ST`
- [ ] `SORT_MEMBER_DOCS`
- [x] `SOURCE_BROWSER`
- [ ] `SOURCE_TOOLTIPS`
- [ ] `SQLITE3_OUTPUT`
- [ ] `SQLITE3_RECREATE_DB`
- [ ] `STRICT_PROTO_MATCHING`
- [ ] `STRIP_CODE_COMMENTS`
- [x] `STRIP_FROM_INC_PATH`
- [x] `STRIP_FROM_PATH`
- [ ] `SUBGROUPING`
- [ ] `TAB_SIZE`
- [ ] `TAGFILES`
- [ ] `TEMPLATE_RELATIONS`
- [ ] `TIMESTAMP`
- [ ] `TOC_EXPAND`
- [ ] `TOC_INCLUDE_HEADINGS`
- [ ] `TREEVIEW_WIDTH`
- [ ] `TYPEDEF_HIDES_STRUCT`
- [ ] `UML_LIMIT_NUM_FIELDS`
- [ ] `UML_LOOK`
- [ ] `USE_HTAGS`
- [ ] `USE_MATHJAX`
- [ ] `USE_MDFILE_AS_MAINPAGE`
- [ ] `USE_PDFLATEX`
- [ ] `VERBATIM_HEADERS`
- [x] `WARNINGS`
- [ ] `WARN_AS_ERROR`
- [ ] `WARN_FORMAT`
- [ ] `WARN_IF_DOC_ERROR`
- [ ] `WARN_IF_INCOMPLETE_DOC`
- [ ] `WARN_IF_UNDOCUMENTED`
- [ ] `WARN_IF_UNDOC_ENUM_VAL`
- [ ] `WARN_LINE_FORMAT`
- [ ] `WARN_LOGFILE`
- [ ] `WARN_NO_PARAMDOC`
- [ ] `XML_NS_MEMB_FILE_SCOPE`
- [ ] `XML_OUTPUT`
- [ ] `XML_PROGRAMLISTING`

## Changelog

See [CHANGELOG](CHANGELOG.md).

## Contributing

See [CONTRIBUTING](CONTRIBUTING.md).

## License

This project is licensed under the [Apache License 2.0](LICENSE).
