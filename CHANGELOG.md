# Changelog

This changelog is based on [Keep a Changelog 1.0.0](https://keepachangelog.com/en/1.0.0).

This project adheres to [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0).

## [0.3.0] - 2024-05-13

### Added

- Created `DoxygenGenerateDefaultHtmlTask`
- Added properties to Doxygen extension and Doxygen tasks

### Changed

- Only add the existing Java source directories in the `doxygen` task created for Java projects
- Add the `src/doxygen` directory in the `doxygen` task created for Java projects, if it exists
- Enable HTML output and disable LaTeX output in the `doxygen` task created for Java projects

## [0.2.0] - 2024-05-11

### Added

- Added properties to `DoxygenExtension` and `DoxygenTask`

## [0.1.1] - 2023-06-05

### Fixed

- Fixed potential `MissingValueException` when tags have a `null` value

## [0.1.0] - 2023-06-05

### Added

- Created `smartefact.doxygen` plugin
- Created `DoxygenTags`
