/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.gradle.doxygen

import java.io.File
import javax.inject.Inject
import org.gradle.api.DefaultTask
import org.gradle.api.file.DirectoryProperty
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.plugins.JavaBasePlugin
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.provider.SetProperty
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import org.gradle.process.ExecOperations
import smartefact.doxygen.DoxyfileWriter
import smartefact.doxygen.DoxygenTags

/**
 * Abstract Doxygen task that can run `doxygen` with a configuration file.
 *
 * This class requires the project to have a [DoxygenExtension].
 *
 * @author Laurent Pireyn
 */
sealed class AbstractDoxygenTask : DefaultTask(), DoxygenConfiguration {
    private val doxygenExtension = project.extensions.getByType(DoxygenExtension::class.java)

    @get:Internal
    abstract override val doxyfile: RegularFileProperty
    init {
        doxyfile.convention(doxygenExtension.doxyfile)
    }

    final override fun doxyfile(file: Any) {
        doxyfile.set(project.file(file))
    }

    @get:InputFile
    @get:Optional
    internal val actualDoxyfile: Provider<File> = project.provider {
        val defaultDoxyfile = project.layout.projectDirectory.file("Doxyfile").asFile
        when {
            doxyfile.isPresent -> doxyfile.get().asFile
            defaultDoxyfile.exists() -> defaultDoxyfile
            else -> null
        }
    }

    @get:Internal
    abstract override val projectName: Property<String>
    init {
        projectName.convention(doxygenExtension.projectName)
    }

    @get:Internal
    abstract override val projectNumber: Property<String>
    init {
        projectNumber.convention(doxygenExtension.projectNumber)
    }

    @get:Internal
    abstract override val projectBrief: Property<String>
    init {
        projectBrief.convention(doxygenExtension.projectBrief)
    }

    @get:Internal
    abstract override val projectLogo: RegularFileProperty
    init {
        projectLogo.convention(doxygenExtension.projectLogo)
    }

    @get:Internal
    abstract override val projectIcon: RegularFileProperty
    init {
        projectIcon.convention(doxygenExtension.projectIcon)
    }

    @get:OutputDirectory
    abstract override val outputDirectory: DirectoryProperty
    init {
        outputDirectory.convention(doxygenExtension.outputDirectory)
    }

    final override fun outputDirectory(directory: Any) {
        outputDirectory.set(project.file(directory))
    }

    @get:Internal
    abstract override val createSubdirectories: Property<Boolean>
    init {
        createSubdirectories.convention(doxygenExtension.createSubdirectories)
    }

    @get:Internal
    abstract override val createSubdirectoriesLevel: Property<Int>
    init {
        createSubdirectoriesLevel.convention(doxygenExtension.createSubdirectoriesLevel)
    }

    @get:Internal
    abstract override val allowUnicodeNames: Property<Boolean>
    init {
        allowUnicodeNames.convention(doxygenExtension.allowUnicodeNames)
    }

    @get:Internal
    abstract override val outputLanguage: Property<String>
    init {
        outputLanguage.convention(doxygenExtension.outputLanguage)
    }

    @get:Internal
    abstract override val briefMemberDescription: Property<Boolean>
    init {
        briefMemberDescription.convention(doxygenExtension.briefMemberDescription)
    }

    @get:Internal
    abstract override val repeatBriefDescription: Property<Boolean>
    init {
        repeatBriefDescription.convention(doxygenExtension.repeatBriefDescription)
    }

    @get:Internal
    abstract override val abbreviateBriefDescription: Property<Boolean>
    init {
        abbreviateBriefDescription.convention(doxygenExtension.abbreviateBriefDescription)
    }

    @get:Internal
    abstract override val alwaysDetailedSection: Property<Boolean>
    init {
        alwaysDetailedSection.convention(doxygenExtension.alwaysDetailedSection)
    }

    @get:Internal
    abstract override val inlineInheritedMembers: Property<Boolean>
    init {
        inlineInheritedMembers.convention(doxygenExtension.inlineInheritedMembers)
    }

    @get:Internal
    abstract override val fullPathNames: Property<Boolean>
    init {
        fullPathNames.convention(doxygenExtension.fullPathNames)
    }

    @get:Internal
    abstract override val stripFromPath: SetProperty<String>

    @get:Internal
    abstract override val stripFromIncPath: SetProperty<String>

    @get:Internal
    abstract override val javadocAutoBrief: Property<Boolean>
    init {
        javadocAutoBrief.convention(doxygenExtension.javadocAutoBrief)
    }

    @get:Internal
    abstract override val javadocBanner: Property<Boolean>
    init {
        javadocBanner.convention(doxygenExtension.javadocBanner)
    }

    @get:Internal
    abstract override val inheritDocs: Property<Boolean>
    init {
        inheritDocs.convention(doxygenExtension.inheritDocs)
    }

    @get:Internal
    abstract override val optimizeOutputJava: Property<Boolean>
    init {
        optimizeOutputJava.convention(doxygenExtension.optimizeOutputJava)
    }

    @get:Internal
    abstract override val markdownSupport: Property<Boolean>
    init {
        markdownSupport.convention(doxygenExtension.markdownSupport)
    }

    @get:Internal
    abstract override val autolinkSupport: Property<Boolean>
    init {
        autolinkSupport.convention(doxygenExtension.autolinkSupport)
    }

    @get:Internal
    abstract override val extractAll: Property<Boolean>
    init {
        extractAll.convention(doxygenExtension.extractAll)
    }

    @get:Internal
    abstract override val extractPrivate: Property<Boolean>
    init {
        extractPrivate.convention(doxygenExtension.extractPrivate)
    }

    @get:Internal
    abstract override val extractPackage: Property<Boolean>
    init {
        extractPackage.convention(doxygenExtension.extractPackage)
    }

    @get:Internal
    abstract override val showFiles: Property<Boolean>
    init {
        showFiles.convention(doxygenExtension.showFiles)
    }

    @get:Internal
    abstract override val quiet: Property<Boolean>
    init {
        quiet.convention(doxygenExtension.quiet)
    }

    @get:Internal
    abstract override val warnings: Property<Boolean>
    init {
        warnings.convention(doxygenExtension.warnings)
    }

    @get:Internal
    abstract override val inputEncoding: Property<String>
    init {
        inputEncoding.convention(doxygenExtension.inputEncoding)
    }

    @get:Internal
    abstract override val filePatterns: SetProperty<String>
    init {
        filePatterns.convention(doxygenExtension.filePatterns)
    }

    @get:Internal
    abstract override val recursive: Property<Boolean>
    init {
        recursive.convention(doxygenExtension.recursive)
    }

    @get:Internal
    abstract override val sourceBrowser: Property<Boolean>
    init {
        sourceBrowser.convention(doxygenExtension.sourceBrowser)
    }

    @get:Internal
    abstract override val inlineSources: Property<Boolean>
    init {
        inlineSources.convention(doxygenExtension.inlineSources)
    }

    @get:Internal
    abstract override val generateHtml: Property<Boolean>
    init {
        generateHtml.convention(doxygenExtension.generateHtml)
    }

    @get:Internal
    abstract override val generateLatex: Property<Boolean>
    init {
        generateLatex.convention(doxygenExtension.generateLatex)
    }

    @get:Internal
    abstract override val enablePreprocessing: Property<Boolean>
    init {
        enablePreprocessing.convention(doxygenExtension.enablePreprocessing)
    }

    @get:Internal
    abstract override val tags: MapProperty<String, Any?>

    @get:Input
    internal val actualTags = project.provider {
        mapOf(
            DoxygenTags.PROJECT_NAME to projectName,
            DoxygenTags.PROJECT_NUMBER to projectNumber,
            DoxygenTags.PROJECT_BRIEF to projectBrief,
            DoxygenTags.PROJECT_LOGO to projectLogo.asFile,
            DoxygenTags.PROJECT_ICON to projectIcon.asFile,
            DoxygenTags.OUTPUT_DIRECTORY to outputDirectory.asFile,
            DoxygenTags.CREATE_SUBDIRS to createSubdirectories,
            DoxygenTags.CREATE_SUBDIRS_LEVEL to createSubdirectoriesLevel,
            DoxygenTags.ALLOW_UNICODE_NAMES to allowUnicodeNames,
            DoxygenTags.OUTPUT_LANGUAGE to outputLanguage,
            DoxygenTags.BRIEF_MEMBER_DESC to briefMemberDescription,
            DoxygenTags.REPEAT_BRIEF to repeatBriefDescription,
            DoxygenTags.ABBREVIATE_BRIEF to abbreviateBriefDescription,
            DoxygenTags.ALWAYS_DETAILED_SEC to alwaysDetailedSection,
            DoxygenTags.INLINE_INHERITED_MEMB to inlineInheritedMembers,
            DoxygenTags.FULL_PATH_NAMES to fullPathNames,
            DoxygenTags.STRIP_FROM_PATH to (doxygenExtension.stripFromPath.get() + stripFromPath.get()).takeIf { it.isNotEmpty() },
            DoxygenTags.STRIP_FROM_INC_PATH to (doxygenExtension.stripFromIncPath.get() + stripFromIncPath.get()).takeIf { it.isNotEmpty() },
            DoxygenTags.JAVADOC_AUTOBRIEF to javadocAutoBrief,
            DoxygenTags.JAVADOC_BANNER to javadocBanner,
            DoxygenTags.INHERIT_DOCS to inheritDocs,
            DoxygenTags.OPTIMIZE_OUTPUT_JAVA to optimizeOutputJava,
            DoxygenTags.MARKDOWN_SUPPORT to markdownSupport,
            DoxygenTags.AUTOLINK_SUPPORT to autolinkSupport,
            DoxygenTags.EXTRACT_ALL to extractAll,
            DoxygenTags.EXTRACT_PRIVATE to extractPrivate,
            DoxygenTags.EXTRACT_PACKAGE to extractPackage,
            DoxygenTags.SHOW_FILES to showFiles,
            DoxygenTags.QUIET to quiet,
            DoxygenTags.WARNINGS to warnings,
            DoxygenTags.INPUT_ENCODING to inputEncoding,
            DoxygenTags.FILE_PATTERNS to filePatterns.get().takeIf { it.isNotEmpty() },
            DoxygenTags.RECURSIVE to recursive,
            DoxygenTags.SOURCE_BROWSER to sourceBrowser,
            DoxygenTags.INLINE_SOURCES to inlineSources,
            DoxygenTags.GENERATE_HTML to generateHtml,
            DoxygenTags.GENERATE_LATEX to generateLatex,
            DoxygenTags.ENABLE_PREPROCESSING to enablePreprocessing,
        ) +
            doxygenExtension.tags.get() +
            createTags() +
            tags.get()
    }

    @get:OutputFile
    internal val configFile = project.layout.buildDirectory.file("tmp/Doxyfile")

    @get:Inject
    internal abstract val execOperations: ExecOperations

    init {
        group = JavaBasePlugin.DOCUMENTATION_GROUP
    }

    protected open fun createTags(): Map<String, Any?> =
        emptyMap()

    @TaskAction
    fun run() {
        // Write configuration file
        val file = configFile.get().asFile.apply { parentFile.mkdirs() }
        val charset = DoxyfileWriter.DEFAULT_CHARSET
        DoxyfileWriter(file.bufferedWriter(charset)).use { writer ->
            writer.writeComment("Generated by Smartefact Doxygen Gradle plugin")
            writer.writeEmptyLine()
            writer.writeTag(DoxygenTags.DOXYFILE_ENCODING, charset.name())
            actualDoxyfile.orNull?.let { doxyfile ->
                writer.writeTag(DoxygenTags.INCLUDE, doxyfile)
            }
            actualTags.get().asSequence()
                .filterNot { (name, _) -> name == DoxygenTags.DOXYFILE_ENCODING }
                .map { (name, value) -> name to if (value is Provider<*>) value.orNull else value }
                .sortedBy { (name, _) -> name }
                .forEach { (name, value) -> writer.writeTag(name, value) }
        }
        // Run doxygen
        execOperations.exec { exec ->
            exec.executable(doxygenExtension.command.get())
            exec.args(createDoxygenOptions(file.absolutePath))
        }
    }

    protected abstract fun createDoxygenOptions(configFilePath: String): Iterable<String>
}
