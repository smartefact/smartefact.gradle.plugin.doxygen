/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.gradle.doxygen

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.plugins.JavaPluginExtension
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.compile.JavaCompile
import smartefact.doxygen.DoxygenTags

/**
 * Doxygen [Plugin].
 *
 * @author Laurent Pireyn
 */
open class DoxygenPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        createProjectExtension(project)
        createDefaultTasks(project)
        project.plugins.withType(JavaPlugin::class.java) {
            configureJavaProject(project)
        }
    }

    private fun createProjectExtension(project: Project): DoxygenExtension =
        project.extensions.create(DoxygenExtension.NAME, DoxygenExtension::class.java, project)

    private fun createDefaultTasks(project: Project) {
        project.tasks.register("doxygenGenerateDefaultHtml", DoxygenGenerateDefaultHtmlTask::class.java)
    }

    private fun configureJavaProject(project: Project) {
        project.tasks.register("doxygen", DoxygenTask::class.java) { doxygenTask ->
            val javaExtension = project.extensions.getByType(JavaPluginExtension::class.java)
            val mainSourceSet = javaExtension.sourceSets.getByName(SourceSet.MAIN_SOURCE_SET_NAME)
            val mainSourceDirectories = mainSourceSet.allJava.sourceDirectories.filter { it.isDirectory }
            val doxygenSourceDirectory = project.layout.projectDirectory.dir("src/doxygen").asFile.takeIf { it.isDirectory }
            val compileJavaTask = project.tasks.named(mainSourceSet.compileJavaTaskName, JavaCompile::class.java).get()
            with(doxygenTask) {
                outputDirectory.set(javaExtension.docsDir.dir("doxygen"))
                briefMemberDescription.set(true)
                fullPathNames.set(false)
                repeatBriefDescription.set(true)
                alwaysDetailedSection.set(false)
                javadocAutoBrief.set(true)
                javadocBanner.set(true)
                optimizeOutputJava.set(true)
                markdownSupport.set(false)
                autolinkSupport.set(false)
                extractAll.set(true)
                showFiles.set(false)
                input.from(mainSourceDirectories)
                doxygenSourceDirectory?.let { input.from(it) }
                inputEncoding.set(compileJavaTask.options.encoding)
                filePatterns.addAll("*.java", "*.md", "*.markdown")
                recursive.set(true)
                generateHtml.set(true)
                generateLatex.set(false)
                enablePreprocessing.set(false)
                tags.putAll(mapOf(
                    DoxygenTags.DIRECTORY_GRAPH to false,
                    DoxygenTags.INLINE_SIMPLE_STRUCTS to false,
                    DoxygenTags.SEPARATE_MEMBER_PAGES to false,
                    DoxygenTags.SHOW_USED_FILES to false,
                    DoxygenTags.SORT_BRIEF_DOCS to true,
                    DoxygenTags.SORT_MEMBER_DOCS to true,
                    DoxygenTags.SORT_MEMBERS_CTORS_1ST to true,
                    DoxygenTags.SUBGROUPING to false
                ))
            }
        }
    }
}
