/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.gradle.doxygen

import java.nio.charset.Charset
import org.gradle.api.file.DirectoryProperty
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.SetProperty

/**
 * Object that has the properties of a Doxygen configuration.
 *
 * @author Laurent Pireyn
 */
interface DoxygenConfiguration {
    /**
     * Doxyfile.
     *
     * This file is included at the beginning of the generated configuration file.
     *
     * Defaults to `Doxyfile` in the project directory, if it exists.
     */
    val doxyfile: RegularFileProperty

    fun doxyfile(file: Any)

    /**
     * Project name.
     *
     * Mapped to the `PROJECT_NAME` tag.
     *
     * Defaults to the project name.
     */
    val projectName: Property<String>

    /**
     * Project number.
     *
     * Mapped to the `PROJECT_NUMBER` tag.
     *
     * Defaults to the project version.
     */
    val projectNumber: Property<String>

    /**
     * Project brief.
     *
     * Mapped to the `PROJECT_BRIEF` tag.
     *
     * Defaults to the project description.
     */
    val projectBrief: Property<String>

    /**
     * Project logo.
     *
     * Mapped to the `PROJECT_LOGO` tag.
     */
    val projectLogo: RegularFileProperty

    /**
     * Project icon.
     *
     * Mapped to the `PROJECT_ICON` tag.
     */
    val projectIcon: RegularFileProperty

    /**
     * Output directory.
     *
     * Mapped to the `OUTPUT_DIRECTORY` tag.
     *
     * Defaults to `$buildDir/doxygen`.
     */
    val outputDirectory: DirectoryProperty

    fun outputDirectory(directory: Any)

    /**
     * Create subdirectories.
     *
     * Mapped to the `CREATE_SUBDIRS` tag.
     */
    val createSubdirectories: Property<Boolean>

    /**
     * Number of created subdirectories.
     *
     * Mapped to the `CREATE_SUBDIRS_LEVEL` tag.
     */
    val createSubdirectoriesLevel: Property<Int>

    /**
     * Allow non-ASCII characters in generated file names.
     *
     * Mapped to the `ALLOW_UNICODE_NAMES` tag.
     */
    val allowUnicodeNames: Property<Boolean>

    /**
     * Output language.
     *
     * Mapped to the `OUTPUT_LANGUAGE` tag.
     */
    val outputLanguage: Property<String>

    /**
     * Include brief member descriptions after members listed in file and class documentation.
     *
     * Mapped to the `BRIEF_MEMBER_DESC` tag.
     */
    val briefMemberDescription: Property<Boolean>

    /**
     * Prepend the brief description of a member before its detailed description.
     *
     * Mapped to the `REPEAT_BRIEF` tag.
     */
    val repeatBriefDescription: Property<Boolean>

    /**
     * Abbreviate brief descriptions.
     *
     * Mapped to the `ABBREVIATE_BRIEF` tag.
     */
    val abbreviateBriefDescription: Property<Boolean>

    /**
     * Always generate a detailed section.
     *
     * Mapped to the `ALWAYS_DETAILED_SEC` tag.
     */
    val alwaysDetailedSection: Property<Boolean>

    /**
     * Include inherited members in class documentation.
     *
     * Mapped to the `INLINE_INHERITED_MEMB` tag.
     */
    val inlineInheritedMembers: Property<Boolean>

    /**
     * Prepend full path before file names in file list.
     *
     * Mapped to the `FULL_PATH_NAMES` tag.
     */
    val fullPathNames: Property<Boolean>

    /**
     * Paths to be stripped from the full path.
     *
     * Mapped to the `STRIP_FROM_PATH` tag.
     */
    val stripFromPath: SetProperty<String>

    /**
     * Paths to be stripped from the include path.
     *
     * Mapped to the `STRIP_FROM_INC_PATH` tag.
     */
    val stripFromIncPath: SetProperty<String>

    /**
     * Interpret comments starting with more than two asterisks as Javadoc-style comments.
     *
     * Mapped to the `JAVADOC_BANNER` tag.
     */
    val javadocBanner: Property<Boolean>

    /**
     * Interpret the first line of a Javadoc-style comment as the brief description.
     *
     * Mapped to the `JAVADOC_AUTOBRIEF` tag.
     */
    val javadocAutoBrief: Property<Boolean>

    /**
     * Make undocumented members inherit the documentation from the members they implement.
     *
     * Mapped to the `INHERIT_DOCS` tag.
     */
    val inheritDocs: Property<Boolean>

    /**
     * Optimize output for Java or Python sources.
     *
     * Mapped to the `OPTIMIZE_OUTPUT_JAVA` tag.
     */
    val optimizeOutputJava: Property<Boolean>

    /**
     * Support Markdown in comments.
     *
     * Mapped to the `MARKDOWN_SUPPORT` tag.
     */
    val markdownSupport: Property<Boolean>

    /**
     * Try to link words that correspond to classes or namespaces to their documentation.
     *
     * Mapped to the `AUTOLINK_SUPPORT` tag.
     */
    val autolinkSupport: Property<Boolean>

    /**
     * Assume all entities are documented.
     *
     * Mapped to the `EXTRACT_ALL` tag.
     */
    val extractAll: Property<Boolean>

    /**
     * Include private members of classes in the documentation.
     *
     * Mapped to the `EXTRACT_PRIVATE` tag.
     */
    val extractPrivate: Property<Boolean>

    /**
     * Include package or internal members in the documentation.
     *
     * Mapped to the `EXTRACT_PACKAGE` tag.
     */
    val extractPackage: Property<Boolean>

    /**
     * Generate the *Files* page.
     *
     * Mapped to the `SHOW_FILES` tag.
     */
    val showFiles: Property<Boolean>

    /**
     * Disable messages generated to standard output by Doxygen.
     *
     * Mapped to the `QUIET` tag.
     */
    val quiet: Property<Boolean>

    /**
     * Disable warning messages generated to standard error by Doxygen.
     *
     * Mapped to the `WARNINGS` tag.
     */
    val warnings: Property<Boolean>

    /**
     * Input encoding.
     *
     * Mapped to the `INPUT_ENCODING` tag.
     */
    val inputEncoding: Property<String>

    fun inputEncoding(charset: Charset) {
        inputEncoding.set(charset.name())
    }

    /**
     * Input file patterns.
     *
     * Mapped to the `FILE_PATTERNS` tag.
     */
    val filePatterns: SetProperty<String>

    /**
     * Search subdirectories for input files.
     *
     * Mapped to the `RECURSIVE` tag.
     */
    val recursive: Property<Boolean>

    /**
     * Generate list of source files.
     *
     * Mapped to the `SOURCE_BROWSER` tag.
     */
    val sourceBrowser: Property<Boolean>

    /**
     * Include the sources in the documentation.
     *
     * Mapped to the `INLINE_SOURCES` tag.
     */
    val inlineSources: Property<Boolean>

    /**
     * Generate HTML output.
     *
     * Mapped to the `GENERATE_HTML` tag.
     */
    val generateHtml: Property<Boolean>

    /**
     * Generate LaTeX output.
     *
     * Mapped to the `GENERATE_LATEX` tag.
     */
    val generateLatex: Property<Boolean>

    /**
     * Evaluate C-preprocessor directives in source and include files.
     *
     * Mapped to the `ENABLE_PREPROCESSING` tag.
     */
    val enablePreprocessing: Property<Boolean>

    /**
     * Doxygen tags.
     */
    val tags: MapProperty<String, Any?>
}
