/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.gradle.doxygen

import java.io.File
import org.gradle.api.Project
import org.gradle.api.file.RegularFileProperty
import java.nio.charset.Charset

/**
 * Doxygen project extension.
 *
 * @author Laurent Pireyn
 */
abstract class DoxygenExtension(
    private val project: Project
) : DoxygenConfiguration {
    final override fun doxyfile(file: Any) {
        doxyfile.set(project.file(file))
    }

    final override fun outputDirectory(directory: Any) {
        outputDirectory.set(project.file(directory))
    }

    /**
     * Doxygen command.
     *
     * Defaults to `/usr/bin/doxygen`.
     */
    abstract val command: RegularFileProperty

    init {
        // TODO: Automatically determine the doxygen command
        command.set(File("/usr/bin/doxygen"))
        projectName.set(project.provider { project.name })
        projectNumber.set(project.provider { project.version.toString().takeIf { it != Project.DEFAULT_VERSION } })
        projectBrief.set(project.provider { project.description })
        outputDirectory.set(project.layout.buildDirectory.dir("doxygen"))
    }

    companion object {
        const val NAME = "doxygen"
    }
}
