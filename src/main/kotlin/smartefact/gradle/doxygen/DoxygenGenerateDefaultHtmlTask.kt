/*
 * Copyright 2023-2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.gradle.doxygen

import org.gradle.api.file.DirectoryProperty
import org.gradle.api.tasks.OutputDirectory

/**
 * Doxygen task that generates the default HTML files.
 *
 * @author Laurent Pireyn
 */
abstract class DoxygenGenerateDefaultHtmlTask : AbstractDoxygenTask() {
    /**
     * Default HTML directory.
     *
     * Defaults to `$buildDir/doxygen/default-html`.
     */
    @get:OutputDirectory
    abstract val defaultHtmlDirectory: DirectoryProperty
    init {
        defaultHtmlDirectory.set(project.layout.buildDirectory.dir("doxygen/default-html"))
    }

    init {
        description = "Generates the default HTML files for Doxygen."
    }

    final override fun createDoxygenOptions(configFilePath: String): Iterable<String> {
        val directory = defaultHtmlDirectory.asFile.get()
        return listOf("-w", "html") +
            listOf("header.html", "footer.html", "stylesheet.css").map { directory.resolve(it).absolutePath } +
            listOf(configFilePath)
    }
}
