/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.doxygen

import java.io.BufferedWriter
import java.io.File

/**
 * Doxyfile writer.
 *
 * @author Laurent Pireyn
 */
internal class DoxyfileWriter(
    private val writer: BufferedWriter
) : AutoCloseable {
    fun writeTag(name: String, value: Any?) {
        fun Any?.toTagValue(): String? =
            when (this) {
                null -> null
                is Boolean -> if (this) "YES" else "NO"
                is Number -> toString()
                is CharSequence -> "\"$this\""
                is File -> absolutePath.toTagValue()
                is Collection<*> -> map { it.toTagValue() }.joinToString(separator = " ")
                else -> toString().toTagValue()
            }

        value.toTagValue()?.let {
            with(writer) {
                write(name)
                write(" = ")
                write(it)
                newLine()
            }
        }
    }

    fun writeComment(comment: String) {
        with (writer) {
            write("# ")
            write(comment)
            newLine()
        }
    }

    fun writeEmptyLine() {
        writer.newLine()
    }

    override fun close() {
        writer.close()
    }

    companion object {
        val DEFAULT_CHARSET = Charsets.UTF_8
    }
}
