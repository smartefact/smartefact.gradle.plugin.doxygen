/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.doxygen

/**
 * Doxygen tags.
 *
 * Based on Doxygen 1.10.0.
 *
 * @author Laurent Pireyn
 */
object DoxygenTags {
    const val ABBREVIATE_BRIEF = "ABBREVIATE_BRIEF"
    const val ALIASES = "ALIASES"
    const val ALLEXTERNALS = "ALLEXTERNALS"
    const val ALLOW_UNICODE_NAMES = "ALLOW_UNICODE_NAMES"
    const val ALPHABETICAL_INDEX = "ALPHABETICAL_INDEX"
    const val ALWAYS_DETAILED_SEC = "ALWAYS_DETAILED_SEC"
    const val AUTOLINK_SUPPORT = "AUTOLINK_SUPPORT"
    const val BINARY_TOC = "BINARY_TOC"
    const val BRIEF_MEMBER_DESC = "BRIEF_MEMBER_DESC"
    const val BUILTIN_STL_SUPPORT = "BUILTIN_STL_SUPPORT"
    const val CALLER_GRAPH = "CALLER_GRAPH"
    const val CALL_GRAPH = "CALL_GRAPH"
    const val CASE_SENSE_NAMES = "CASE_SENSE_NAMES"
    const val CHM_FILE = "CHM_FILE"
    const val CHM_INDEX_ENCODING = "CHM_INDEX_ENCODING"
    const val CITE_BIB_FILES = "CITE_BIB_FILES"
    const val CLANG_ADD_INC_PATHS = "CLANG_ADD_INC_PATHS"
    const val CLANG_ASSISTED_PARSING = "CLANG_ASSISTED_PARSING"
    const val CLANG_DATABASE_PATH = "CLANG_DATABASE_PATH"
    const val CLANG_OPTIONS = "CLANG_OPTIONS"
    const val CLASS_GRAPH = "CLASS_GRAPH"
    const val COLLABORATION_GRAPH = "COLLABORATION_GRAPH"
    const val COMPACT_LATEX = "COMPACT_LATEX"
    const val COMPACT_RTF = "COMPACT_RTF"
    const val CPP_CLI_SUPPORT = "CPP_CLI_SUPPORT"
    const val CREATE_SUBDIRS = "CREATE_SUBDIRS"
    const val CREATE_SUBDIRS_LEVEL = "CREATE_SUBDIRS_LEVEL"
    const val DIAFILE_DIRS = "DIAFILE_DIRS"
    const val DIA_PATH = "DIA_PATH"
    const val DIRECTORY_GRAPH = "DIRECTORY_GRAPH"
    const val DIR_GRAPH_MAX_DEPTH = "DIR_GRAPH_MAX_DEPTH"
    const val DISABLE_INDEX = "DISABLE_INDEX"
    const val DISTRIBUTE_GROUP_DOC = "DISTRIBUTE_GROUP_DOC"
    const val DOCBOOK_OUTPUT = "DOCBOOK_OUTPUT"
    const val DOCSET_BUNDLE_ID = "DOCSET_BUNDLE_ID"
    const val DOCSET_FEEDNAME = "DOCSET_FEEDNAME"
    const val DOCSET_FEEDURL = "DOCSET_FEEDURL"
    const val DOCSET_PUBLISHER_ID = "DOCSET_PUBLISHER_ID"
    const val DOCSET_PUBLISHER_NAME = "DOCSET_PUBLISHER_NAME"
    const val DOTFILE_DIRS = "DOTFILE_DIRS"
    const val DOT_CLEANUP = "DOT_CLEANUP"
    const val DOT_COMMON_ATTR = "DOT_COMMON_ATTR"
    const val DOT_EDGE_ATTR = "DOT_EDGE_ATTR"
    const val DOT_FONTPATH = "DOT_FONTPATH"
    const val DOT_GRAPH_MAX_NODES = "DOT_GRAPH_MAX_NODES"
    const val DOT_IMAGE_FORMAT = "DOT_IMAGE_FORMAT"
    const val DOT_MULTI_TARGETS = "DOT_MULTI_TARGETS"
    const val DOT_NODE_ATTR = "DOT_NODE_ATTR"
    const val DOT_NUM_THREADS = "DOT_NUM_THREADS"
    const val DOT_PATH = "DOT_PATH"
    const val DOT_UML_DETAILS = "DOT_UML_DETAILS"
    const val DOT_WRAP_THRESHOLD = "DOT_WRAP_THRESHOLD"
    const val DOXYFILE_ENCODING = "DOXYFILE_ENCODING"
    const val ECLIPSE_DOC_ID = "ECLIPSE_DOC_ID"
    const val ENABLED_SECTIONS = "ENABLED_SECTIONS"
    const val ENABLE_PREPROCESSING = "ENABLE_PREPROCESSING"
    const val ENUM_VALUES_PER_LINE = "ENUM_VALUES_PER_LINE"
    const val EXAMPLE_PATH = "EXAMPLE_PATH"
    const val EXAMPLE_PATTERNS = "EXAMPLE_PATTERNS"
    const val EXAMPLE_RECURSIVE = "EXAMPLE_RECURSIVE"
    const val EXCLUDE = "EXCLUDE"
    const val EXCLUDE_PATTERNS = "EXCLUDE_PATTERNS"
    const val EXCLUDE_SYMBOLS = "EXCLUDE_SYMBOLS"
    const val EXCLUDE_SYMLINKS = "EXCLUDE_SYMLINKS"
    const val EXPAND_AS_DEFINED = "EXPAND_AS_DEFINED"
    const val EXPAND_ONLY_PREDEF = "EXPAND_ONLY_PREDEF"
    const val EXTENSION_MAPPING = "EXTENSION_MAPPING"
    const val EXTERNAL_GROUPS = "EXTERNAL_GROUPS"
    const val EXTERNAL_PAGES = "EXTERNAL_PAGES"
    const val EXTERNAL_SEARCH = "EXTERNAL_SEARCH"
    const val EXTERNAL_SEARCH_ID = "EXTERNAL_SEARCH_ID"
    const val EXTRACT_ALL = "EXTRACT_ALL"
    const val EXTRACT_ANON_NSPACES = "EXTRACT_ANON_NSPACES"
    const val EXTRACT_LOCAL_CLASSES = "EXTRACT_LOCAL_CLASSES"
    const val EXTRACT_LOCAL_METHODS = "EXTRACT_LOCAL_METHODS"
    const val EXTRACT_PACKAGE = "EXTRACT_PACKAGE"
    const val EXTRACT_PRIVATE = "EXTRACT_PRIVATE"
    const val EXTRACT_PRIV_VIRTUAL = "EXTRACT_PRIV_VIRTUAL"
    const val EXTRACT_STATIC = "EXTRACT_STATIC"
    const val EXTRA_PACKAGES = "EXTRA_PACKAGES"
    const val EXTRA_SEARCH_MAPPINGS = "EXTRA_SEARCH_MAPPINGS"
    const val EXT_LINKS_IN_WINDOW = "EXT_LINKS_IN_WINDOW"
    const val FILE_PATTERNS = "FILE_PATTERNS"
    const val FILE_VERSION_FILTER = "FILE_VERSION_FILTER"
    const val FILTER_PATTERNS = "FILTER_PATTERNS"
    const val FILTER_SOURCE_FILES = "FILTER_SOURCE_FILES"
    const val FILTER_SOURCE_PATTERNS = "FILTER_SOURCE_PATTERNS"
    const val FORCE_LOCAL_INCLUDES = "FORCE_LOCAL_INCLUDES"
    const val FORMULA_FONTSIZE = "FORMULA_FONTSIZE"
    const val FORMULA_MACROFILE = "FORMULA_MACROFILE"
    const val FORTRAN_COMMENT_AFTER = "FORTRAN_COMMENT_AFTER"
    const val FULL_PATH_NAMES = "FULL_PATH_NAMES"
    const val FULL_SIDEBAR = "FULL_SIDEBAR"
    const val GENERATE_AUTOGEN_DEF = "GENERATE_AUTOGEN_DEF"
    const val GENERATE_BUGLIST = "GENERATE_BUGLIST"
    const val GENERATE_CHI = "GENERATE_CHI"
    const val GENERATE_DEPRECATEDLIST = "GENERATE_DEPRECATEDLIST"
    const val GENERATE_DOCBOOK = "GENERATE_DOCBOOK"
    const val GENERATE_DOCSET = "GENERATE_DOCSET"
    const val GENERATE_ECLIPSEHELP = "GENERATE_ECLIPSEHELP"
    const val GENERATE_HTML = "GENERATE_HTML"
    const val GENERATE_HTMLHELP = "GENERATE_HTMLHELP"
    const val GENERATE_LATEX = "GENERATE_LATEX"
    const val GENERATE_LEGEND = "GENERATE_LEGEND"
    const val GENERATE_MAN = "GENERATE_MAN"
    const val GENERATE_PERLMOD = "GENERATE_PERLMOD"
    const val GENERATE_QHP = "GENERATE_QHP"
    const val GENERATE_RTF = "GENERATE_RTF"
    const val GENERATE_SQLITE3 = "GENERATE_SQLITE3"
    const val GENERATE_TAGFILE = "GENERATE_TAGFILE"
    const val GENERATE_TESTLIST = "GENERATE_TESTLIST"
    const val GENERATE_TODOLIST = "GENERATE_TODOLIST"
    const val GENERATE_TREEVIEW = "GENERATE_TREEVIEW"
    const val GENERATE_XML = "GENERATE_XML"
    const val GRAPHICAL_HIERARCHY = "GRAPHICAL_HIERARCHY"
    const val GROUP_GRAPHS = "GROUP_GRAPHS"
    const val GROUP_NESTED_COMPOUNDS = "GROUP_NESTED_COMPOUNDS"
    const val HAVE_DOT = "HAVE_DOT"
    const val HHC_LOCATION = "HHC_LOCATION"
    const val HIDE_COMPOUND_REFERENCE = "HIDE_COMPOUND_REFERENCE"
    const val HIDE_FRIEND_COMPOUNDS = "HIDE_FRIEND_COMPOUNDS"
    const val HIDE_IN_BODY_DOCS = "HIDE_IN_BODY_DOCS"
    const val HIDE_SCOPE_NAMES = "HIDE_SCOPE_NAMES"
    const val HIDE_UNDOC_CLASSES = "HIDE_UNDOC_CLASSES"
    const val HIDE_UNDOC_MEMBERS = "HIDE_UNDOC_MEMBERS"
    const val HIDE_UNDOC_RELATIONS = "HIDE_UNDOC_RELATIONS"
    const val HTML_CODE_FOLDING = "HTML_CODE_FOLDING"
    const val HTML_COLORSTYLE = "HTML_COLORSTYLE"
    const val HTML_COLORSTYLE_GAMMA = "HTML_COLORSTYLE_GAMMA"
    const val HTML_COLORSTYLE_HUE = "HTML_COLORSTYLE_HUE"
    const val HTML_COLORSTYLE_SAT = "HTML_COLORSTYLE_SAT"
    const val HTML_COPY_CLIPBOARD = "HTML_COPY_CLIPBOARD"
    const val HTML_DYNAMIC_MENUS = "HTML_DYNAMIC_MENUS"
    const val HTML_DYNAMIC_SECTIONS = "HTML_DYNAMIC_SECTIONS"
    const val HTML_EXTRA_FILES = "HTML_EXTRA_FILES"
    const val HTML_EXTRA_STYLESHEET = "HTML_EXTRA_STYLESHEET"
    const val HTML_FILE_EXTENSION = "HTML_FILE_EXTENSION"
    const val HTML_FOOTER = "HTML_FOOTER"
    const val HTML_FORMULA_FORMAT = "HTML_FORMULA_FORMAT"
    const val HTML_HEADER = "HTML_HEADER"
    const val HTML_INDEX_NUM_ENTRIES = "HTML_INDEX_NUM_ENTRIES"
    const val HTML_OUTPUT = "HTML_OUTPUT"
    const val HTML_PROJECT_COOKIE = "HTML_PROJECT_COOKIE"
    const val HTML_STYLESHEET = "HTML_STYLESHEET"
    const val IDL_PROPERTY_SUPPORT = "IDL_PROPERTY_SUPPORT"
    const val IGNORE_PREFIX = "IGNORE_PREFIX"
    const val IMAGE_PATH = "IMAGE_PATH"
    const val INCLUDE = "@INCLUDE"
    const val INCLUDED_BY_GRAPH = "INCLUDED_BY_GRAPH"
    const val INCLUDE_FILE_PATTERNS = "INCLUDE_FILE_PATTERNS"
    const val INCLUDE_GRAPH = "INCLUDE_GRAPH"
    const val INCLUDE_PATH = "INCLUDE_PATH"
    const val INHERIT_DOCS = "INHERIT_DOCS"
    const val INLINE_GROUPED_CLASSES = "INLINE_GROUPED_CLASSES"
    const val INLINE_INFO = "INLINE_INFO"
    const val INLINE_INHERITED_MEMB = "INLINE_INHERITED_MEMB"
    const val INLINE_SIMPLE_STRUCTS = "INLINE_SIMPLE_STRUCTS"
    const val INLINE_SOURCES = "INLINE_SOURCES"
    const val INPUT = "INPUT"
    const val INPUT_ENCODING = "INPUT_ENCODING"
    const val INPUT_FILE_ENCODING = "INPUT_FILE_ENCODING"
    const val INPUT_FILTER = "INPUT_FILTER"
    const val INTERACTIVE_SVG = "INTERACTIVE_SVG"
    const val INTERNAL_DOCS = "INTERNAL_DOCS"
    const val JAVADOC_AUTOBRIEF = "JAVADOC_AUTOBRIEF"
    const val JAVADOC_BANNER = "JAVADOC_BANNER"
    const val LATEX_BATCHMODE = "LATEX_BATCHMODE"
    const val LATEX_BIB_STYLE = "LATEX_BIB_STYLE"
    const val LATEX_CMD_NAME = "LATEX_CMD_NAME"
    const val LATEX_EMOJI_DIRECTORY = "LATEX_EMOJI_DIRECTORY"
    const val LATEX_EXTRA_FILES = "LATEX_EXTRA_FILES"
    const val LATEX_EXTRA_STYLESHEET = "LATEX_EXTRA_STYLESHEET"
    const val LATEX_FOOTER = "LATEX_FOOTER"
    const val LATEX_HEADER = "LATEX_HEADER"
    const val LATEX_HIDE_INDICES = "LATEX_HIDE_INDICES"
    const val LATEX_MAKEINDEX_CMD = "LATEX_MAKEINDEX_CMD"
    const val LATEX_OUTPUT = "LATEX_OUTPUT"
    const val LAYOUT_FILE = "LAYOUT_FILE"
    const val LOOKUP_CACHE_SIZE = "LOOKUP_CACHE_SIZE"
    const val MACRO_EXPANSION = "MACRO_EXPANSION"
    const val MAKEINDEX_CMD_NAME = "MAKEINDEX_CMD_NAME"
    const val MAN_EXTENSION = "MAN_EXTENSION"
    const val MAN_LINKS = "MAN_LINKS"
    const val MAN_OUTPUT = "MAN_OUTPUT"
    const val MAN_SUBDIR = "MAN_SUBDIR"
    const val MARKDOWN_ID_STYLE = "MARKDOWN_ID_STYLE"
    const val MARKDOWN_SUPPORT = "MARKDOWN_SUPPORT"
    const val MATHJAX_CODEFILE = "MATHJAX_CODEFILE"
    const val MATHJAX_EXTENSIONS = "MATHJAX_EXTENSIONS"
    const val MATHJAX_FORMAT = "MATHJAX_FORMAT"
    const val MATHJAX_RELPATH = "MATHJAX_RELPATH"
    const val MATHJAX_VERSION = "MATHJAX_VERSION"
    const val MAX_DOT_GRAPH_DEPTH = "MAX_DOT_GRAPH_DEPTH"
    const val MAX_INITIALIZER_LINES = "MAX_INITIALIZER_LINES"
    const val MSCFILE_DIRS = "MSCFILE_DIRS"
    const val MSCGEN_TOOL = "MSCGEN_TOOL"
    const val MULTILINE_CPP_IS_BRIEF = "MULTILINE_CPP_IS_BRIEF"
    const val NUM_PROC_THREADS = "NUM_PROC_THREADS"
    const val OBFUSCATE_EMAILS = "OBFUSCATE_EMAILS"
    const val OPTIMIZE_FOR_FORTRAN = "OPTIMIZE_FOR_FORTRAN"
    const val OPTIMIZE_OUTPUT_FOR_C = "OPTIMIZE_OUTPUT_FOR_C"
    const val OPTIMIZE_OUTPUT_JAVA = "OPTIMIZE_OUTPUT_JAVA"
    const val OPTIMIZE_OUTPUT_SLICE = "OPTIMIZE_OUTPUT_SLICE"
    const val OPTIMIZE_OUTPUT_VHDL = "OPTIMIZE_OUTPUT_VHDL"
    const val OUTPUT_DIRECTORY = "OUTPUT_DIRECTORY"
    const val OUTPUT_LANGUAGE = "OUTPUT_LANGUAGE"
    const val PAPER_TYPE = "PAPER_TYPE"
    const val PDF_HYPERLINKS = "PDF_HYPERLINKS"
    const val PERLMOD_LATEX = "PERLMOD_LATEX"
    const val PERLMOD_MAKEVAR_PREFIX = "PERLMOD_MAKEVAR_PREFIX"
    const val PERLMOD_PRETTY = "PERLMOD_PRETTY"
    const val PLANTUML_CFG_FILE = "PLANTUML_CFG_FILE"
    const val PLANTUML_INCLUDE_PATH = "PLANTUML_INCLUDE_PATH"
    const val PLANTUML_JAR_PATH = "PLANTUML_JAR_PATH"
    const val PREDEFINED = "PREDEFINED"
    const val PROJECT_BRIEF = "PROJECT_BRIEF"
    const val PROJECT_ICON = "PROJECT_ICON"
    const val PROJECT_LOGO = "PROJECT_LOGO"
    const val PROJECT_NAME = "PROJECT_NAME"
    const val PROJECT_NUMBER = "PROJECT_NUMBER"
    const val PYTHON_DOCSTRING = "PYTHON_DOCSTRING"
    const val QCH_FILE = "QCH_FILE"
    const val QHG_LOCATION = "QHG_LOCATION"
    const val QHP_CUST_FILTER_ATTRS = "QHP_CUST_FILTER_ATTRS"
    const val QHP_CUST_FILTER_NAME = "QHP_CUST_FILTER_NAME"
    const val QHP_NAMESPACE = "QHP_NAMESPACE"
    const val QHP_SECT_FILTER_ATTRS = "QHP_SECT_FILTER_ATTRS"
    const val QHP_VIRTUAL_FOLDER = "QHP_VIRTUAL_FOLDER"
    const val QT_AUTOBRIEF = "QT_AUTOBRIEF"
    const val QUIET = "QUIET"
    const val RECURSIVE = "RECURSIVE"
    const val REFERENCED_BY_RELATION = "REFERENCED_BY_RELATION"
    const val REFERENCES_LINK_SOURCE = "REFERENCES_LINK_SOURCE"
    const val REFERENCES_RELATION = "REFERENCES_RELATION"
    const val REPEAT_BRIEF = "REPEAT_BRIEF"
    const val RESOLVE_UNNAMED_PARAMS = "RESOLVE_UNNAMED_PARAMS"
    const val RTF_EXTENSIONS_FILE = "RTF_EXTENSIONS_FILE"
    const val RTF_HYPERLINKS = "RTF_HYPERLINKS"
    const val RTF_OUTPUT = "RTF_OUTPUT"
    const val RTF_STYLESHEET_FILE = "RTF_STYLESHEET_FILE"
    const val SEARCHDATA_FILE = "SEARCHDATA_FILE"
    const val SEARCHENGINE = "SEARCHENGINE"
    const val SEARCHENGINE_URL = "SEARCHENGINE_URL"
    const val SEARCH_INCLUDES = "SEARCH_INCLUDES"
    const val SEPARATE_MEMBER_PAGES = "SEPARATE_MEMBER_PAGES"
    const val SERVER_BASED_SEARCH = "SERVER_BASED_SEARCH"
    const val SHORT_NAMES = "SHORT_NAMES"
    const val SHOW_FILES = "SHOW_FILES"
    const val SHOW_GROUPED_MEMB_INC = "SHOW_GROUPED_MEMB_INC"
    const val SHOW_HEADERFILE = "SHOW_HEADERFILE"
    const val SHOW_INCLUDE_FILES = "SHOW_INCLUDE_FILES"
    const val SHOW_NAMESPACES = "SHOW_NAMESPACES"
    const val SHOW_USED_FILES = "SHOW_USED_FILES"
    const val SIP_SUPPORT = "SIP_SUPPORT"
    const val SITEMAP_URL = "SITEMAP_URL"
    const val SKIP_FUNCTION_MACROS = "SKIP_FUNCTION_MACROS"
    const val SORT_BRIEF_DOCS = "SORT_BRIEF_DOCS"
    const val SORT_BY_SCOPE_NAME = "SORT_BY_SCOPE_NAME"
    const val SORT_GROUP_NAMES = "SORT_GROUP_NAMES"
    const val SORT_MEMBERS_CTORS_1ST = "SORT_MEMBERS_CTORS_1ST"
    const val SORT_MEMBER_DOCS = "SORT_MEMBER_DOCS"
    const val SOURCE_BROWSER = "SOURCE_BROWSER"
    const val SOURCE_TOOLTIPS = "SOURCE_TOOLTIPS"
    const val SQLITE3_OUTPUT = "SQLITE3_OUTPUT"
    const val SQLITE3_RECREATE_DB = "SQLITE3_RECREATE_DB"
    const val STRICT_PROTO_MATCHING = "STRICT_PROTO_MATCHING"
    const val STRIP_CODE_COMMENTS = "STRIP_CODE_COMMENTS"
    const val STRIP_FROM_INC_PATH = "STRIP_FROM_INC_PATH"
    const val STRIP_FROM_PATH = "STRIP_FROM_PATH"
    const val SUBGROUPING = "SUBGROUPING"
    const val TAB_SIZE = "TAB_SIZE"
    const val TAGFILES = "TAGFILES"
    const val TEMPLATE_RELATIONS = "TEMPLATE_RELATIONS"
    const val TIMESTAMP = "TIMESTAMP"
    const val TOC_EXPAND = "TOC_EXPAND"
    const val TOC_INCLUDE_HEADINGS = "TOC_INCLUDE_HEADINGS"
    const val TREEVIEW_WIDTH = "TREEVIEW_WIDTH"
    const val TYPEDEF_HIDES_STRUCT = "TYPEDEF_HIDES_STRUCT"
    const val UML_LIMIT_NUM_FIELDS = "UML_LIMIT_NUM_FIELDS"
    const val UML_LOOK = "UML_LOOK"
    const val USE_HTAGS = "USE_HTAGS"
    const val USE_MATHJAX = "USE_MATHJAX"
    const val USE_MDFILE_AS_MAINPAGE = "USE_MDFILE_AS_MAINPAGE"
    const val USE_PDFLATEX = "USE_PDFLATEX"
    const val VERBATIM_HEADERS = "VERBATIM_HEADERS"
    const val WARNINGS = "WARNINGS"
    const val WARN_AS_ERROR = "WARN_AS_ERROR"
    const val WARN_FORMAT = "WARN_FORMAT"
    const val WARN_IF_DOC_ERROR = "WARN_IF_DOC_ERROR"
    const val WARN_IF_INCOMPLETE_DOC = "WARN_IF_INCOMPLETE_DOC"
    const val WARN_IF_UNDOCUMENTED = "WARN_IF_UNDOCUMENTED"
    const val WARN_IF_UNDOC_ENUM_VAL = "WARN_IF_UNDOC_ENUM_VAL"
    const val WARN_LINE_FORMAT = "WARN_LINE_FORMAT"
    const val WARN_LOGFILE = "WARN_LOGFILE"
    const val WARN_NO_PARAMDOC = "WARN_NO_PARAMDOC"
    const val XML_NS_MEMB_FILE_SCOPE = "XML_NS_MEMB_FILE_SCOPE"
    const val XML_OUTPUT = "XML_OUTPUT"
    const val XML_PROGRAMLISTING = "XML_PROGRAMLISTING"
}
